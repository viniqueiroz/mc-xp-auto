const cheerio = require("cheerio");
const qs = require('qs');

function getSurveyId(data) {
    const $ = cheerio.load(data);
    return $('form').attr('action')
}

function getCoupomData(data) {
    const $ = cheerio.load(data);
    const code = $('#FinishIncentiveCodeBox', 'form').text();
    const expirationDate = $('#FinishExpirationDateBox', 'form').text();
    return { code, expirationDate }
}
async function nextStep({ stepNumber, previousStepResponse, body, axiosInstance }) {
    let formId = getSurveyId(previousStepResponse.data);
    console.log(`${stepNumber} Form ID:`, formId);

    const response = await postRequest({ axiosInstance, formId, body });
    return response
}

async function postRequest({ axiosInstance, formId, body }) {
    const response = await axiosInstance.post(`/${formId}`, qs.stringify(body), {
        withCredentials: true,
    });

    return response
}

module.exports = {
    getSurveyId,
    getCoupomData,
    postRequest,
    nextStep
}