function generateBot() {
    const Telegraf = require('telegraf');
    const TelegrafInlineMenu = require('telegraf-inline-menu');
    const Cupom = require('./getCupom')
    const menu = new TelegrafInlineMenu(ctx => `🍧 Ei ${ctx.from.first_name}! Selecione o Estabelecimento :`);

    menu.setCommand('cupom');

    menu.simpleButton('Mc Rio Vermelho!', '22.932.687/0004-89', {
        doFunc: (ctx) => {
            ctx.reply('Aguarde um momento, estou gerando o cupom!');
            Cupom.generate(ctx.match[0]).then((result) => {
                if (result) {
                    try {
                        ctx.reply(result.code);
                        ctx.replyWithPhoto({ source: result.image }).catch(err => {
                            ctx.reply('Ocorreu um erro, tente novamente!');
                            console.log(err);
                        })
                    } catch (error) {
                        console.log(error);
                        ctx.reply('Ocorreu um erro, tente novamente!');
                    }
                } else {
                    ctx.reply('Ocorreu um erro, tente novamente!');
                }
            }).catch(error => ctx.reply('Ocorreu um erro, tente novamente!'))
        }
    });

    menu.simpleButton('Mc Shopping Barra!', '42.591.651/0698-52', {
        doFunc: (ctx) => {
            ctx.reply('Aguarde um momento, estou gerando o cupom!');
            Cupom.generate(ctx.match[0]).then((result) => {
                if (result) {
                    try {
                        ctx.reply(result.code);
                        ctx.replyWithPhoto({ source: result.image }).catch(err => {
                            ctx.reply('Ocorreu um erro, tente novamente!');
                            console.log(err);
                        })
                    } catch (error) {
                        console.log(error);
                        ctx.reply('Ocorreu um erro, tente novamente!');
                    }
                } else {
                    ctx.reply('Ocorreu um erro, tente novamente!');
                }
            })
        }
    });

    menu.simpleButton('Mc Shopping Paralela!', '42.591.651/1191-16', {
        doFunc: (ctx) => {
            ctx.reply('Aguarde um momento, estou gerando o cupom!');
            Cupom.generate(ctx.match[0], true).then((result) => {
                if (result) {
                    try {
                        ctx.reply(result.code);
                        ctx.replyWithPhoto({ source: result.image }).catch(err => {
                            ctx.reply('Ocorreu um erro, tente novamente!');
                            console.log(err);
                        })
                    } catch (error) {
                        console.log(error);
                        ctx.reply('Ocorreu um erro, tente novamente!');
                    }
                } else {
                    ctx.reply('Ocorreu um erro, tente novamente!');
                }
            })
        }
    });

    menu.simpleButton('Mc Costa Azul!', '22.932.687/0002-17', {
        doFunc: (ctx) => {
            ctx.reply('Aguarde um momento, estou gerando o cupom!');
            Cupom.generate(ctx.match[0], true).then((result) => {
                if (result) {
                    try {
                        ctx.reply(result.code);
                        ctx.replyWithPhoto({ source: result.image }).catch(err => {
                            ctx.reply('Ocorreu um erro, tente novamente!');
                            console.log(err);
                        })
                    } catch (error) {
                        console.log(error);
                        ctx.reply('Ocorreu um erro, tente novamente!');
                    }
                } else {
                    ctx.reply('Ocorreu um erro, tente novamente!');
                }
            })
        }
    });

    menu.simpleButton('Mc Salvador Shopping!', '08.752.246/0001-21', {
        doFunc: (ctx) => {
            ctx.reply('Aguarde um momento, estou gerando o cupom!');
            Cupom.generate(ctx.match[0], true).then((result) => {
                if (result) {
                    try {
                        ctx.reply(result.code);
                        ctx.replyWithPhoto({ source: result.image }).catch(err => {
                            ctx.reply('Ocorreu um erro, tente novamente!');
                            console.log(err);
                        })
                    } catch (error) {
                        console.log(error);
                        ctx.reply('Ocorreu um erro, tente novamente!');
                    }
                } else {
                    ctx.reply('Ocorreu um erro, tente novamente!');
                }
            })
        }
    });

    const bot = new Telegraf(process.env.BOT_TOKEN);
    bot.use(menu.init());
    bot.hears(/\b(?:sorvetinho)/i, (ctx) => ctx.reply('Só digitar o comando /cupom'));
    bot.hears(/\b(?:meu filho)/i, (ctx) => {
        if (ctx.from.username == "viniciusmq") {
            ctx.reply('Te amo Papai ❤️')
        } else {
            ctx.reply('Você não é o Papai 🔪')
        }
    });
    bot.hears(/\b(?:gay)/i, (ctx) => ctx.reply('Com certeza é Júnio 🏳️‍🌈'));
    bot.launch();

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
};

module.exports.generateBot = generateBot;