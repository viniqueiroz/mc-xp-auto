const axios = require('axios');
const { getCoupomData, nextStep } = require('./form-utils');
const { wrapper } = require('axios-cookiejar-support');
const { CookieJar } = require('tough-cookie');
const dayjs = require('dayjs');
const Jimp = require('jimp');

async function generate(cnpj) {
    const url = 'https://mcexperienciasurvey.com';
    const headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': '*/*',
    }
    try {
        const date = dayjs().subtract(3, 'day');
        const dateFormatted = date.format('YYYYMMDD');
        const datePicker = date.format('DD/MM/YYYY');
        console.log('Data Formatada', dateFormatted);
        console.log('Data Picker', datePicker);

        const jar = new CookieJar();
        const axiosInstance = wrapper(axios.create({ baseURL: url, headers, withCredentials: true, jar }));

        // FIRST STEP
        const response = await axiosInstance.get('', '?AspxAutoDetectCookieSupport=0');

        const firstBody = {
            'AspxAutoDetectCookieSupport': '0',
            'JavaScriptEnabled': '1',
            'FIP': 'True',
            'P': '1',
            'Index_CountryPicker': '3'
        }
        const firstResponse = await nextStep({ stepNumber: '1#', previousStepResponse: response, body: firstBody, axiosInstance })
        // ---------------

        // SECOND STEP   TODO: Add support to date, hour and CNPJ
        const secondBody = {
            'AspxAutoDetectCookieSupport': '0',
            'JavaScriptEnabled': '1',
            'FIP': 'True',
            'InputCNPJ': cnpj, //'22.932.687/0004-89',
            'Index_VisitDateFormattedDate': dateFormatted,
            'Index_VisitDateDatePicker': datePicker,
            'InputHour': '08',
            'InputMinute': '30',
            'Index_OptIn': '1',
            'NextButton': 'Iniciar',
        }
        const secondResponse = await nextStep({ stepNumber: '2#', previousStepResponse: firstResponse, body: secondBody, axiosInstance })
        // ---------------

        // THIRD STEP
        const thirdBody = {
            'R000001': '1',
            'IoNF': '1',
            'PostedFNS': 'R000001',
        }
        const thirdResponse = await nextStep({ stepNumber: '3#', previousStepResponse: secondResponse, body: thirdBody, axiosInstance })
        // ---------------

        // FOURTH STEP
        const fourthBody = {
            'R000002': '2',
            'IoNF': '2',
            'PostedFNS': 'R000002',
        }
        const fourthResponse = await nextStep({ stepNumber: '4#', previousStepResponse: thirdResponse, body: fourthBody, axiosInstance })
        // ---------------


        // FIFTH STEP
        const fifthBody = {
            'R000004': '5',
            'IoNF': '4',
            'PostedFNS': 'R000004',
        }
        const fifthResponse = await nextStep({ stepNumber: '5#', previousStepResponse: fourthResponse, body: fifthBody, axiosInstance })
        // ---------------

        // SIXTH STEP
        const sixthBody = {
            'R000006': '5',
            'R000009': '5',
            'R000008': '5',
            'R000011': '5',
            'R000007': '5',
            'R000012': '5',
            'IoNF': '11',
            'PostedFNS': 'R000006|R000009|R000008|R000011|R000007|R000012',
        }
        const sixthResponse = await nextStep({ stepNumber: '6#', previousStepResponse: fifthResponse, body: sixthBody, axiosInstance })
        // ---------------

        // SEVENTH STEP
        const seventhBody = {
            'R000013': '1',
            'IoNF': '20',
            'PostedFNS': 'R000013',
        }
        const seventhResponse = await nextStep({ stepNumber: '7#', previousStepResponse: sixthResponse, body: seventhBody, axiosInstance })
        // ---------------

        // EIGHTH STEP
        const eighthBody = {
            'R000015': '5',
            'R000016': '10',
            'IoNF': '35',
            'PostedFNS': 'R000015|R000016',
        }
        const eighthResponse = await nextStep({ stepNumber: '8#', previousStepResponse: seventhResponse, body: eighthBody, axiosInstance })
        // ---------------

        // NINETH STEP
        const ninethBody = {
            'S000019': 'Lugar agradável, pedido em ordem',
            'IoNF': '38',
            'PostedFNS': 'S000019',
        }
        const ninethResponse = await nextStep({ stepNumber: '9#', previousStepResponse: eighthResponse, body: ninethBody, axiosInstance })
        // ---------------

        // TENTH STEP
        const tenthBody = {
            'R000020': '2',
            'IoNF': '39',
            'PostedFNS': 'R000020',
        }
        const tenthResponse = await nextStep({ stepNumber: '10#', previousStepResponse: ninethResponse, body: tenthBody, axiosInstance })
        // ---------------

        // ELEVENTH STEP
        const eleventhBody = {
            'S000036': 'Osvaldo',
            'S000028': 'Oliveira',
            'S000035': '71999998888',
            'S000033': 'osvaldo@hotmail.com',
            'S000034': 'osvaldo@hotmail.com',
            'IoNF': '53',
            'PostedFNS': 'S000036|S000028|S000035|S000033|S000034',
        }
        const eleventhResponse = await nextStep({ stepNumber: '11#', previousStepResponse: tenthResponse, body: eleventhBody, axiosInstance })
        // ---------------

        // FINAL STEP
        const finalBody = {
            'R000040': '2',
            'IoNF': '54',
            'PostedFNS': 'R000040',
        }
        const finalResponse = await nextStep({ stepNumber: 'Final', previousStepResponse: eleventhResponse, body: finalBody, axiosInstance })
        // ---------------

        const cupom = getCoupomData(finalResponse.data);

        const codeFont = await Jimp.loadFont(Jimp.FONT_SANS_16_BLACK);
        const dateFont = await Jimp.loadFont(Jimp.FONT_SANS_10_BLACK);
        const jimpImage = await Jimp.read('./template/Incentive_BRA.png');

        jimpImage.print(codeFont, 280, 199, cupom.code);
        jimpImage.print(dateFont, 320, 218, cupom.expirationDate);
        const image = await jimpImage.getBufferAsync(Jimp.MIME_PNG);
        return { ...cupom, image };
    } catch (error) {
        console.log(error);
        return false;
    }
};

module.exports.generate = generate;