//Servidor web

//Express é a framework utilizado para o Back-End.
require('dotenv').config();
// const cors = require('cors');
const express = require('express');
const app = express();
const routes = require('./routes');
const bot = require('./botCommands');

bot.generateBot();

//Permite Acesso externo na aplicação
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With,x-access-token,Content-Type, Accept");
    next();
});

//Pasta utilizada para o frontEnd vai ser a pasta public
// app.use('/', express.static(__dirname + '/codes'));

routes(app);
// Quando quiser rodar a aplicação sem especificar a porta use esse comando
// app.listen(process.env.PORT_APP, function() {
//     console.log('Rodando porta' + process.env.PORT_APP);
// });

app.listen(process.env.PORT || 3000, function () {
    console.log("Servidor rodando na porta %d em modo %s", this.address().port, app.settings.env);
});
